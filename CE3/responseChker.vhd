
library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    --use ieee.numeric_std.all;
    use ieee.math_real.all;
    

    entity responseChker is
        port (
            valueIN     : in  std_logic_vector(11 downto 0);
            sqrtIN      : in std_logic_vector(11 downto 0);
            sqrtOUTconv : out real;
            sqrtOUT     : out real;            
            sqrtDIFF    : out real   
        );
    end responseChker;


architecture beh of responseChker is

    function realVal (
        constant qFVector   : in std_logic_vector; -- Q-Format coded vector
        constant msbMeaning : in integer           -- msb meaning (2^?)
        ) return real is
       
            variable resu : real;
            variable e    : integer;
            
    begin
            e := msbMeaning;
            
            if qFVector(qFVector'high)='1' then
                resu := -(2.0**e);
                
            else
                resu := 0.0;
                
            end if;
            
            for i in qFVector'high-1 downto qFVector'low loop
                e := e-1;
                
                if qFVector(i)='1' then
                    resu := resu + (2.0**e);
                    
                end if;
            end loop; 
            
            return resu;
    end function realVal;

begin
    

    characteristic:
        process(valueIN, sqrtIN) is
            
            variable sqrtReal   : real := 0.0;
            variable valueSLV   : std_logic_vector(11 downto 0);
            variable valueReal  : real := 0.0;
          --  variable sqrtDIFF   : real := 0.0;
            variable sqrtIN_v   : std_logic_vector(11 downto 0);
            variable sqrtINReal : real := 0.0;
            
        begin
        
            valueSLV   := valueIN;  
            sqrtIN_v   := sqrtIN;
            sqrtINReal := realVal(sqrtIN, 0);
            valueReal  := realVal(valueSLV, 0);
            
            if(valueSLV(valueSLV'left) = '1') then
                valueReal := -valueReal;
                
            end if;
            
            sqrtReal := sqrt(valueReal);
            
            if(valueSLV(valueSLV'left) = '1') then
                sqrtReal := -sqrtReal;
                
            end if;
            
          --  if(valueIN(valueIN'left) = '1') then
          --  sqrtReal := -sqrtReal;    
          --  end if;
    
            sqrtOUT     <= sqrtReal;
            sqrtDIFF    <= sqrtINReal - sqrtReal;        
            sqrtOUTconv <= sqrtINReal;
           
        end process;
        
end architecture beh;

