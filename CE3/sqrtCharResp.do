onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /datapath_tb/clk
add wave -noupdate -format Logic /datapath_tb/reset
add wave -noupdate -format Logic /datapath_tb/start
add wave -noupdate -format Logic /datapath_tb/ready
add wave -noupdate -format Analog-Step -height 74 -offset 2048.0 -scale 0.017094017094017096 /datapath_tb/value
add wave -noupdate -format Analog-Backstep -height 200 -offset 2048.0 -scale 0.0478515625 /datapath_tb/sqrt
add wave -noupdate -format Literal -radix decimal /datapath_tb/sqrtdiff
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1073458 ns} 0}
configure wave -namecolwidth 144
configure wave -valuecolwidth 75
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {503337 ns}
