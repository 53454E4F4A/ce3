
library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    USE ieee.std_logic_unsigned.all; 
    

entity dataGen is
   generic (
        nob      : natural :=  16;  
        waittime : natural := 100  
      ); --generic
    port (
       value : out std_logic_vector(nob-1 downto 0);
       start : out std_logic;
       ready : in  std_logic
   
    );
    constant nop        : natural := nob * nob;  -- Number Of Permutations 
    constant waittime_c : natural := waittime;
end dataGen;


architecture beh of dataGen is
begin
    
    characteristic:
        process is
            variable value_v : std_logic_vector(nob-1 downto 0) := (others => '0');

        begin
            value_v(value_v'left) := '1';
                
            for i in 4096 downto 1 loop
                value   <= value_v;
          
                wait until ready = '1';
                start   <= '1';
                wait until ready = '0';
                start   <= '0';
                value_v := std_logic_vector(unsigned(value_v) + 1);  
            end loop;	

            wait;
            
        end process;

end architecture beh;

