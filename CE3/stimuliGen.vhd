library work;
	use work.all;
library ieee;
	use ieee.std_logic_1164.all;
	
ENTITY stimuliGen IS
	PORT (
		
		clk	: OUT	STD_LOGIC;
		res	: OUT	STD_LOGIC
	);
END ENTITY stimuliGen;

ARCHITECTURE stimuliGen_a OF stimuliGen IS
	
	SIGNAL run_s : BOOLEAN;

BEGIN

    runTime:
	    PROCESS IS
	    BEGIN
		    run_s <= TRUE;
	        WAIT FOR 1000000 ns;
		    run_s <= FALSE;
		    WAIT;
	    END PROCESS runTime;
	
	
    reset:
	    PROCESS IS
	    BEGIN
		    res <= '0';
		    WAIT;
	    END PROCESS reset;


    clock:
        PROCESS IS
        BEGIN
            WAIT FOR 1 ns;
            WHILE run_s LOOP
                clk <= '1';
                WAIT FOR 10 ns;
                clk <= '0';
                WAIT FOR 10 ns;
            END LOOP;
            WAIT;
        END PROCESS clock;
	
END ARCHITECTURE stimuliGen_a;

