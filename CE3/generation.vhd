
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;


entity generation is   
    generic (
        nnob      : natural :=  16;
        wwaittime : natural := 100
    ); --generic    
    port (
        ready : in  std_logic;
        clk   : out std_logic;
        res   : out std_logic;
        start : out std_logic;
        value : out std_logic_vector(nnob-1 downto 0)
    );
end entity generation;


architecture beh of generation is

		
    component dataGen is
        generic (
            nob      : natural := nnob;  
            waittime : natural := wwaittime  
        ); --generic
        port (
            value : out std_logic_vector(nob-1 downto 0);
            start : out std_logic;
            ready : in  std_logic
        );
    end component dataGen;
    for all : dataGen use entity work.dataGen(beh); 
   
   
    component stimuliGen is
        port (
            clk	  : out	std_logic;
            res	  : out	std_logic
        );
    end component stimuligen;
    for all : stimuliGen use entity work.stimuliGen(stimuliGen_a);

begin

    datagen_i : dataGen
    generic map(
        nob => nnob     
    )
    port map(
        value => value,
        ready => ready,
        start => start
    )
    ; --] datagen
  
  
    stimuli_i : stimuliGen
    port map(
        clk => clk,
        res => res
    )
    ;--]stimuli

end beh;

