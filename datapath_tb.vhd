
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;


entity datapath_tb is       
end entity datapath_tb;


architecture beh of datapath_tb is


    component SquareRootComputer is
        generic (
            wwidth    : natural :=  12
        ); --generic    
        port (
            x     : in  std_logic_vector(11 downto 0);
            req   : in  std_logic;
            clk   : in  std_logic;
            sres  : in  std_logic;
            fx    : out std_logic_vector(11 downto 0);
            rdy   : out std_logic
        )
    ;
    end component SquareRootComputer;
    for all : SquareRootComputer use entity work.SquareRootComputer(beh);
        
	
	component generation is
	    generic (
	        nnob      : natural := 16;
	        wwaittime : natural := 100
        ); --generic
        port (
            ready : in  std_logic;
            clk   : out std_logic;
            res   : out std_logic;
            start : out std_logic;
            value : out std_logic_vector(nnob-1 downto 0)
        )
    ;
    end component generation;
    for all : generation use entity work.generation(beh);
		
		
    component responseChker is
        port (
            valueIN     : in  std_logic_vector(11 downto 0);
            sqrtIN      : in std_logic_vector(11 downto 0);
            sqrtOUT     : out real;
            sqrtOUTconv : out real;
            sqrtDIFF    : out real
        )
    ;
    end component responseChker;
    for all : responseChker use entity work.responseChker(beh);
    
	     
    signal value             : std_logic_vector(11 downto 0) := (others => '0');
    signal clk               : std_logic;
    signal reset             : std_logic;
    signal start             : std_logic;
    signal ready             : std_logic;
    signal sqrtDIFF          : real;
    signal sqrt              : std_logic_vector(11 downto 0);
    signal sqrtOUT           : real;
    signal sqrtOUTconv       : real;

begin

    responseChker_i : responseChker
        port map (
            sqrtDIFF    => sqrtDIFF,
            valueIN     => value,
            sqrtIN      => sqrt,
            sqrtOUT     => sqrtOUT,
            sqrtOUTconv => sqrtOUTconv
        )
    ;--]responseChker;
    

    genration_i : generation
        generic map (
            nnob => 12
        )
        port map (
            clk   => clk,
            res   => reset,
            start => start,
            value => value,
            ready => ready
        )
    ;--]generation;
    
	
    SqareRootComputer_i : SquareRootComputer
        port map(
            x     => value,
            req   => start,
            clk   => clk,
            sres  => reset,
            fx    => sqrt,
            rdy   => ready
        )
    ;--]SquareRootComputer;

end beh;

